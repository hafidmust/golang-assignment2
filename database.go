package assignment223maret

import (
	"database/sql"
	"time"
)

var db *sql.DB

type Item struct {
	ItemID      int    `json:"item_id"`
	ItemCode    string `json:"item_code"`
	Description string `json:"description"`
	Quantity    int    `json:"quantity"`
	OrderID     int    `json:"order_id"`
}

type Order struct {
	OrderID      int       `json:"order_id"`
	CustomerName string    `json:"customer_name"`
	OrderedAt    time.Time `json:"ordered_at"`
	Items        []Item    `json:"items"`
}

func initializeDB() error {
	var err error
	db, err = sql.Open("postgres", "postgres://postgres:postgres@localhost/orders_db?sslmode=disable")
	if err != nil {
		return err
	}
	return db.Ping()
}

func getOrders() ([]Order, error) {
	rows, err := db.Query("SELECT order_id, customer_name, ordered_at FROM orders")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var orders []Order
	for rows.Next() {
		var order Order
		if err := rows.Scan(&order.OrderID, &order.CustomerName, &order.OrderedAt); err != nil {
			return nil, err
		}
		orders = append(orders, order)
	}
	return orders, nil
}

func createOrder(order *Order) error {
	tx, err := db.Begin()
	if err != nil {
		return err
	}
	defer tx.Rollback()

	orderedAt := time.Now()
	err = tx.QueryRow("INSERT INTO orders (customer_name, ordered_at) VALUES ($1, $2) RETURNING order_id", order.CustomerName, orderedAt).Scan(&order.OrderID)
	if err != nil {
		return err
	}

	for i := range order.Items {
		_, err := tx.Exec("INSERT INTO items (item_code, description, quantity, order_id) VALUES ($1, $2, $3, $4)", order.Items[i].ItemCode, order.Items[i].Description, order.Items[i].Quantity, order.OrderID)
		if err != nil {
			return err
		}
	}

	if err := tx.Commit(); err != nil {
		return err
	}

	return nil
}
