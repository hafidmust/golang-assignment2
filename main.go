package assignment223maret

import "github.com/gin-gonic/gin"

func wrapper(f func(c *gin.Context) (string, error)) gin.HandlerFunc {

	return func(c *gin.Context) {
		_, err := f(c)
		if err != nil {
			c.JSON(503, gin.H{"status": err})
			return
		}
		c.JSON(200, gin.H{"status": "OK"})
	}
}

func main() {
	if err := initializeDB(); err != nil {
		panic(err)
	}
	defer db.Close()

	r := gin.Default()
	r.GET("/orders", wrapper(getOrders))
	r.POST("/orders", wrapper(createOrder))
	r.Run(":8080")
}
